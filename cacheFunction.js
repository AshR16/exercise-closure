function cacheFunction(cb) {
    let cache = {}
    
    return (argument)=>{
    if(cache[argument]){
        console.log("Already cached")
        return cache[argument]
    }else{
        console.log("Add to the cache")
        return cache[argument] = cb(argument)
    }
}
}

module.exports = cacheFunction
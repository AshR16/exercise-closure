const cacheFunction = require("../cacheFunction")

function cb(argument){
    return argument * argument
}

let result = cacheFunction(cb)

console.log(result(2))
console.log(result(3))
console.log(result(2))
function limitFunctionCallCount(cb, n) {

    let counter = 0
    function count(){
     if(counter<n){
        counter++
        return cb()
     }else{
         return null
    }
    }

   return count
}

module.exports = limitFunctionCallCount
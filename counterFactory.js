function counterFactory() {

    let counter=0
    let obj = {}
    obj.increment = ()=>{
        counter++
        return counter
    }
    obj.decrement = ()=>{
        counter--
        return counter
    }

    return obj
}

module.exports = counterFactory